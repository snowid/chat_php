﻿## 聊天软件后台
### 安装
将以下代码添加到 composer.json中的require，重要的是后三句。如末安装compser 请联系作者将完整安装包发给你。
```$xslt
"require": {
    "php": ">=5.6.0",
    "workerman/gateway-worker": "^3.0",
    "workerman/mysql": "^1.0",
    "workerman/gatewayclient": "^3.0"
}
```
>composer install

以下以linux为例，执行前请先检测[workermanPHP扩展](http://doc.workerman.net/install/requirement.html)及Redis和PHP Redis扩展 windows用户请参见[手册](http://doc2.workerman.net/)的 Windows系统快速开始
> php start.php start

start.php 文件和ws文件可以放到任何目录，只要改一下引用即可。当然你也可以修改ws文件夹名称，只需要start.php修改好路径。

如果是小程序、APP等需要用到wss 433端口，建议使用[利用nginx/apache代理wss](http://doc.workerman.net/faq/secure-websocket-server.html)

支持多端口同时运行，复制/ws/chat/文件，修改好start_businessworker.php中的各项参数，到start.php中增加引用

### 工作逻辑
当用户发来消息后，将消息转成数组 ，数组中须有 $data['module']) $data['action']，将直接调用相应的类和方法。