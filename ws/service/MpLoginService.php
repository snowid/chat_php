<?php
namespace service;
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2018/11/2
 * Time: 23:52
 */
use \GatewayWorker\Lib\Gateway;
class MpLoginService
{
    //用户所在组
    static protected $group= 'mp_login';

    /**
     * 用户初始连接时
     * $_SESSION 会保存存用户信息 用户所在类 用户类型
     * */
    public static function onOpen($client_id,$data ,$db,$redis)
    {
        $uid=self::$group.'_'.$data['session_id'];

        /*将其它多余页面的连接关闭*/
        $arr=Gateway::getClientIdByUid( $uid);
        foreach ($arr as $item){
            Gateway::sendToClient($item,json_encode(['type'=>'Onclose'],true));
            Gateway::unbindUid($item, $uid);
        }

        Gateway::joinGroup($client_id, self::$group);
        Gateway::bindUid($client_id, $uid);
        Gateway::sendToUid($uid,json_encode(['type'=>'Onopen'],true));
    }


}